package model.data.structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {

	private int tamaņoLista;
	private NodoDoble<T> primero;
	private NodoDoble<T> ultimo;
	private NodoDoble<T> actual;
	private ListaDobleEncadenada<T> siguiente;

	public ListaDobleEncadenada()
	{
		tamaņoLista=0;
		primero=null;
		ultimo=null;
		actual=primero;
	}

	@Override
	public Iterator<T> iterator() 
	{
		return new Iterator<T>() {

			public boolean hasNext()
			{
				if (actual == null)
				{
					return false;
				}
				else if (actual.darNext()== null)
				{
					return false;
				}
				else{
					return true;
				}
			}

			@Override
			public T next() {
				T temp = actual.darElemento();
				actual = actual.darNext();
				return temp;
			}

			@Override
			public void remove() {
			}
		};
	}

	@Override
	public void agregarElementoFinal(T elem) 
	{
		if(!iterator().hasNext()){
			primero= new NodoDoble<T>(elem,0);
			ultimo= primero;
			actual=primero;
			tamaņoLista++;
		}
		else{

			NodoDoble<T> elAgregar= new NodoDoble<T>(elem, ultimo.darPosicion()+1);
			ultimo.modificarSiguiente(elAgregar, elAgregar.darPosicion());
			elAgregar.modificarAnterior(ultimo, ultimo.darPosicion());
			ultimo=elAgregar;
			tamaņoLista++;
		}
	}

	public T eliminar(T elem)
	{
		T elemt = null;
	
		boolean termino = false;
		while(iterator().hasNext() && !termino)
		{
			if(darElementoPosicionActual().equals(elem))
			{
				actual.modificarSiguiente(actual.darNext(), actual.darPosicion());
				actual.modificarAnterior(actual.darAnterior(), actual.darPosicion());
				elemt = darElementoPosicionActual();
				tamaņoLista--;
				termino = true;
			}
		}
		return elemt;
	}
	@Override
	public T darElemento(int pos) 
	{
		while (iterator().hasNext()){

			if (actual.darPosicion()==pos){
				return actual.darElemento();
			}

			iterator().next();
		}

		return null;
	}


	@Override
	public int darNumeroElementos() 
	{
		return tamaņoLista;
	}

	@Override
	public T darElementoPosicionActual() 
	{
		return actual.darElemento();
	}

	@Override
	public boolean avanzarSiguientePosicion() 
	{
		Iterator<T> iterador = iterator();

		if (!iterador.hasNext()){
			return false;
		}
		else{
			if (actual.darNext()==null ){
				return false;
			}
			else{
				actual=actual.darNext();
				return true;
			}
		}


	}
	public void moverActualInicio()
	{
		actual = primero;

	}
	public void moverIzquierda()
	{
		if (primero!=null && actual.darAnterior()!=null && actual!=primero){
			actual=actual.darAnterior();
		}
	}

	@Override
	public boolean retrocederPosicionAnterior() 
	{

		Iterator<T> iterador = iterator();
		if (!iterador.hasNext()){
			return false;
		}
		else{
			if (actual.darAnterior()==null || actual.equals(primero)){
				return false;
			}
			else{
				actual=actual.darAnterior();
				return true;
			}
		}

	}
 
	public ListaDobleEncadenada<T> darSiguiente()
	{
		return siguiente;
		
	}

}
