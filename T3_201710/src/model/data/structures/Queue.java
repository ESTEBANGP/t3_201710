package model.data.structures;

public class Queue<T>
{
	private ListaDobleEncadenada<T> lista;
	private int  tama�o;
	
	public Queue()
	{
		lista = null;
		tama�o = 0;
	}
	
	public void enqueue(T elem)
	{
		ListaDobleEncadenada<T> nuevo = new ListaDobleEncadenada<>();
		
		if (isEmpty())
		{
			lista = nuevo;
		}
		else
		{
			nuevo.agregarElementoFinal(elem);
			lista = nuevo;
		}
		tama�o++;
	}
	
	public T dequeue ()
	{
		T elemt = null;
		if(!isEmpty())
		{
			lista.moverActualInicio();
			elemt = lista.eliminar(lista.darElementoPosicionActual());
			tama�o--;
		}
		return elemt;
	}
	public boolean isEmpty()
	{
		return lista==null;
	}
	
	public int size()
	{
		return tama�o;
	}
	
	public T nodo()
	{
		return lista.darElementoPosicionActual();
	}
}
