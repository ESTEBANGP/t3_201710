package model.data.structures;

public class Stack<T>
{
	private ListaDobleEncadenada<T> lista;
	
	private int tama�o;
//Stack<ListaDobleEncadenada> Stack = new Stack<ListaDobleEncadenada>();
	
	public Stack()
	{
		lista = null;
		tama�o = 0;
	}
	
	public void push(T item)
	{
		ListaDobleEncadenada<T> nodo = new ListaDobleEncadenada<>();
		
		nodo.agregarElementoFinal(item);
		
		if (isEmptly())
		{
			lista = nodo;
		}
		else
		{
			nodo.agregarElementoFinal(item);
			lista = nodo;
		}
		
		tama�o++;
	}
	public T pop()
	{	
		T cima  = null;
		if (!isEmptly())
		{
			cima = lista.eliminar(lista.darElementoPosicionActual());
			tama�o--;
		}
		return cima;
	}
	public boolean isEmptly()
	{
		return lista==null;
	}
	public int size()
	{
	return tama�o;	
	}
	public T  nodo()
	{
		return lista.darElementoPosicionActual();
	}
}
