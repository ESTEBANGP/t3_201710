/**
 * 
 */
package model.data.structures;

public class NodoDoble<T> {
	
	private NodoDoble<T> next;
	private NodoDoble<T> back;
	private T elemento;
	private int posicion;
	
	public NodoDoble(T pElemento,int posicion)
	{
		elemento = pElemento;
		next = null;
		back = null;
	}
	public T darElemento()
	{
		return elemento;
	}
	
	public int darPosicion()
	{
		return posicion;
	}
	
	public NodoDoble<T> darNext()
	{
		return next;
	}
	
	public NodoDoble<T> darAnterior()
	{
		return back;
	}
	
	public void modificarSiguiente(NodoDoble<T> nodo, int pPosicion)
	{
		next = nodo;
		posicion = pPosicion;
	}
	
	public void modificarAnterior(NodoDoble<T> nodo, int pPosicion)
	{
		back = nodo;
		posicion = pPosicion;
	}
	
	public void agregarElemento(T pElemento){
		elemento = pElemento;
	}


}
