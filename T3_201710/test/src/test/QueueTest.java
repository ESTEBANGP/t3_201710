package test;

import model.data.structures.Queue;
import junit.framework.TestCase;

public class QueueTest  extends TestCase

{

	private Queue<String> cola;
	private Queue<String> nuevo;
	
	private void setupEscenario1()
	{
		cola = new Queue<String>();
		
		cola.enqueue("item1");
		cola.enqueue("item2");
		cola.enqueue("item3");
		cola.enqueue("item4");
	}
	
	private void setupEscenario2()
	{
		nuevo = new Queue<String>();
	}
	
	public void testAgregar()
	{
		setupEscenario1();
		cola.enqueue("item5");
		assertEquals(5, cola.size());
	}
	
	public  void testEliminar()
	{
		setupEscenario1();
		cola.dequeue();
		assertEquals("item4", cola.nodo());
	}
	
	public void testVacio()
	{
		setupEscenario2();
		nuevo.size();
		assertEquals(0, nuevo.size());
	}
}
