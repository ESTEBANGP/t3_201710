package test;

import model.data.structures.ListaDobleEncadenada;
import model.data.structures.Stack;
import junit.framework.TestCase;

public class StackTest extends TestCase
{

	private  Stack<String> pila;
	private Stack<String> nuevo;

	private void setupEscenario1()
	{
		pila = new Stack<String>();

		pila.push("item1");
		pila.push("item2");
		pila.push("item3");
		pila.push("item4");
	}

	public  void  testAgregar()
	{
		setupEscenario1();
		pila.push("item5");
		assertEquals(5, pila.size());
	}

	public void testEliminar()
	{
		setupEscenario1();
		pila.pop();
		assertEquals("item4", pila.nodo());
	}
	
	public void  setupEscenario2()
	{
		 nuevo = new Stack<String>();
	}
	
	public void testVacio()
	{
		setupEscenario2();
		nuevo.size();
		assertEquals(0, nuevo.size());
	}
}
